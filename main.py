""" Packages"""
# 3rd party modules
import graph

# core modules
import time

if __name__ == '__main__':

    print('Creating environment data')
    start = time.clock()

    # generate graph (adjacency matrix) and the cost matrix
    adj, cost = graph.random(50, 1, 0.9)
    print(adj), print(cost)

    finish = time.clock() - start
    print('Running time {} sec'.format(finish))

    graph.plot(adj)
