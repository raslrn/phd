# 3rd party modules
import numpy as np
import matplotlib.pyplot as plt

# personal modules
import ksp
import analysis

# core modules
import random
import time

# environment parameters
n = 10
k = int(n/2)

#np.random.seed(0)

print('Creating environment data ...')
start = time.clock()

env = ksp.KspEnv(n, k, T=10)
s = env.reset()

print('Agent interacting with the environment ...')

for t in range(env.T):
    env.render()

    a = random.randint(0, env.k)

    s, r, done = env.step(a)

    if done:
        break

finish = time.clock() - start
print('Running time {} sec'.format(finish))

#for reg in env.log:
#    print('{}'.format(reg))
