# 3rd party modules
import numpy as np
import time

# core modules
from itertools import combinations


class QlAgent:
    def __init__(self, env, gamma=0.99, alpha=0.5, theta=0.001, epsilon=0.2):
        self.env = env
        self.gamma = gamma
        self.alpha = alpha
        self.theta = theta
        self.epsilon = epsilon

        states = combinations(range(env.n), env.k)
        nS = len(states)
        nA = env.n*env.k
        self.Q = self.table(nS, nA)

    def policy(self):

        pi = np.ones(self.env.k, dtype=float) * self.epsilon/self.env.k
        greed_action = np.argmax(self.Q[self.s])
        pi[greed_action] = 1 - self.epsilon + pi[greed_action]
        a = np.random.choice(np.arange(len(pi)), p=pi)

        return a

    def table(self, nS, nA):
        self.Q = np.zeros([nS, nA])
        print(self.Q)
