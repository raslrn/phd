import gym

env = gym.make('CartPole-v0')

for ep in range(20):

    obs = env.reset()
    for t in range(100):

        env.render()
        print(obs)
        act = env.action_space.sample()

        obs, rew, done, info = env.step(act)

        if done:

            print("Episode finished after {} timesteps".format(t+1))
            break