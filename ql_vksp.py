# 3rd party modules
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

# core modules
import time

# personal modules
from vksp import VisualKspEnv
from ql_agent import QlAgent
import analysis

print('Creating environment data ...')
start = time.clock()

# reproducibility
np.random.seed(0)

# n must be a perfect square
env = VisualKspEnv(n=9, k=3, T=1000, is_render=True)
agent = QlAgent(env)

reward_data, R = [], []
acc_rew_size = 100

# start agent-env interaction
for t in range(env.T):

    if env.is_render:
        env.render()

    a = agent.policy()

    o, r, done = env.step(a)

    # compute the accumulated reward
    reward_data.append(r)

    _, div = divmod(t, acc_rew_size)

    if div == 0 and t > 0:
        acc_rew = np.sum(reward_data)
        print('ep: {} reward (acc): {}'.format(t, acc_rew))
        if agent.is_plot:
            agent.plot_reward(t, acc_rew)
        reward_data.clear()
        R.append(acc_rew)

    if done:
        acc_rew = np.sum(reward_data)
        print('ep: {} reward (acc): {}'.format(t, acc_rew))
        if agent.is_plot:
            agent.plot_reward(t, acc_rew)
        R.append(acc_rew)


finish = time.clock() - start
print('Running time {} sec'.format(finish))

analysis.plot_accumulated_reward(R)
